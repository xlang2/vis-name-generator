import csv
import re
from urllib.request import urlopen
from bs4 import BeautifulSoup



urls = [ "http://ieeevis.org/year/2020/info/papers-sessions", "http://ieeevis.org/year/2021/info/papers-sessions"]


def scrape(url):
    name = "vis" + url.split("/")[-3]
    page = urlopen(url)
    html_bytes = page.read()
    html = html_bytes.decode("utf-8")
    soup = BeautifulSoup(html, "html.parser")


    titles = soup.find_all("strong")
    titles = map(lambda t: t.text, titles)
    titles = filter(lambda t: "CDT" not in t, titles)
    titles = filter(lambda t: "October" not in t, titles)

    with open(f"{name}.tsv", 'w', newline='', encoding="utf-8") as f:
        csv_writer = csv.writer(f, delimiter=';')
        for title in titles:
            csv_writer.writerow([title])

for url in urls:
    scrape(url)
