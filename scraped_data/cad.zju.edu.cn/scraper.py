import csv
import re
from urllib.request import urlopen
from bs4 import BeautifulSoup



conferences = ["scivis", "infovis", "vast"]
base_url = "http://www.cad.zju.edu.cn/home/vagblog/vispapers/"
years = range(2012, 2019)

urls = [ "http://www.cad.zju.edu.cn/home/vagblog/vispapers/vis2019.html"]
for conference in conferences:
    for year in years: 
        url = f"{base_url}/{conference}{year}.html"
        urls.append(url)




def scrape(url):
    name = url.split("/")[-1].split(".")[0]
    page = urlopen(url)
    html_bytes = page.read()
    html = html_bytes.decode("utf-8")
    soup = BeautifulSoup(html, "html.parser")


    titles = soup.find_all("span", {"class": "projecttitle"})
    titles = map(lambda t: t.text, titles)
    titles = map(lambda t: re.sub(r'\([^)]*\)', '', t), titles);
    titles = map(lambda t: re.sub(r'\[[^)]*\]', '', t), titles);
    titles = map(lambda t: t.strip(), titles);

    with open(f"{name}.tsv", 'w', newline='', encoding="utf-8") as f:
        csv_writer = csv.writer(f, delimiter=';')
        for title in titles:
            csv_writer.writerow([title])

for url in urls:
    scrape(url)
