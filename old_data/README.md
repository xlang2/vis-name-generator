# Web of Science search

The list of publications has been exported from the WOS website 2022-07-04 with the following parameters:

```
Conference: IEEE AND VIS
OR
Conference: Visualization conference
```
