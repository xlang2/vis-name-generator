import json
from urllib import request


from urllib.request import urlopen
import requests
bin_list = json.load(open("../answers/bins.json", "r"))
answer_array = []
for i, bin in enumerate(bin_list):
    print(f"Downloading {bin} ({i+1}/{len(bin_list)}")
    url = f'https://api.jsonbin.io/v3/b/{bin}'
    headers = {
        'X-Master-Key': '$2b$10$805/JQOuky/OXY4kybXYlujQqZT7LjmndEOVe7n2ZyzniZN/pWs3G'
    }

    req = requests.get(url, json=None, headers=headers)
    answer_array.append(req.json())
    print(req.json()["record"])

json.dump(answer_array, open("./output.json", "w"))
