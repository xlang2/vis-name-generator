import { adjectives } from "./adjectives";
import { intros } from "./intros";
import { nouns } from "./nouns";

import { scorePronouncability } from "../pronounceable";

function capitalizeFirstLetter(string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

function abbreviate(string) {
  let arr = string.split(" ");
  let res = "";
  for (const word of arr) {
    let letter = word.charAt(0).toUpperCase();
    res = `${res}${letter}`;
  }
  return res;
}

function getRandomElement(list) {
  let index = getRandomInt(list.length);
  return list[index];
}


export function getRandomPaperName() {

  let paperName = ""
  let intro = getRandomElement(intros);
  let noun = getRandomElement(nouns);
  let adjective = getRandomElement(adjectives);
  let paperType = getRandomInt(100);
  let vis = getRandomElement([
    "Vis",
    "Viz",
    "Show",
    "Draw",
    "Paint",
    "See",
    "Viewer",
    "X",
  ]);
  let fillerWord = getRandomElement(["of", "for", "using"]);
  if (paperType < 40) {
    paperName = `${capitalizeFirstLetter(adjective)} ${intro} ${fillerWord} ${noun}`;
  } else if (paperType < 80) {
    paperName = `${capitalizeFirstLetter(intro)} ${fillerWord} ${adjective} ${noun}`;
  } else if (paperType < 95) {
    paperName = `${capitalizeFirstLetter(noun)}${vis}: ${capitalizeFirstLetter(
      intro
    )} ${fillerWord} ${adjective} ${noun} `;
  } else {
    paperName = `${capitalizeFirstLetter(adjective)} ${noun} visualization: State of the art`;
  }

  if (scorePronouncability(paperName) > 0.15 && paperName.search(":") === -1) {
    return `${abbreviate(paperName)}: ${paperName}`;
  } else {
    return paperName;
  }
}
