import React, { useState } from 'react'
import { DefaultButton } from "@fluentui/react"
import { getRandomPaperName } from '../modules/paper-name-generator'

function PaperNameGenerator() {
    const [randomPaper, setRandomPaper] = useState(getRandomPaperName())

    const divStyle = {
        background: '#AFADAA',
        'font-size': '1rem',
        color: 'white',
        border: '0px',
        'border-radius': '5px',
    };

    return (
        <div className="generator">
            <div className="paper-name">{randomPaper}</div>
            <span className="warning">Yes, it's randomly generated from a large dictionary. We are not responsible for any rejects! :)</span>
            <div>
                <DefaultButton style={divStyle}
                    onClick={() => setRandomPaper(getRandomPaperName())}
                >Another paper!</DefaultButton>
            </div>
        </div>
    )
}

export default PaperNameGenerator
