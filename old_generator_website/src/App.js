import './App.css';
import PaperNameGenerator from "./components/PaperNameGenerator";
import bgLeft from "./images/top_left.png";
import bgRight from "./images/top_right.png";
import bottom from "./images/bottom.png";
import visitlabLogo from "./images/visitlab_logo.png";

function App() {
  return (
    <>
      <img src={bgLeft} alt="" className="background bg-left" ></img>
      <img src={bgRight} alt="" className="background bg-right" ></img>
      <img src={bottom} alt="" className="background bg-bottom" ></img>
      <div className="content">
        <h2>
          <a href="https://visitlab.fi.muni.cz/"><img alt="Visitlab logo"  src={visitlabLogo} className="logo-text" /></a>
        </h2>
        <h2>  
          <span className="wish">wishes you happy holidays and a great new year!</span>
        </h2>
        <span className="blurb">Have some inspiration for your next visualization paper!</span>
        <PaperNameGenerator></PaperNameGenerator>
      </div>
    </>
  );
}

export default App;
